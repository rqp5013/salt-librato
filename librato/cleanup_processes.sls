{% from "librato/map.jinja" import librato with context %}
{%- set role = salt['grains.get']('roles', '') %}

{%- for ele in ['petabucket-test','petabucket-prod'] %}
{%- if role == ele %}
{{ librato.plugin_config_path }}/cleanup_processes.conf:
  file.managed:
    - template: jinja
    - user: root
    - group: root
    - mode: 0644
    - source: salt://librato/files/cleanup_processes.conf.jinja
{%- endif %}
{%- endfor %}

include:
  - librato.agent
