{% from "librato/map.jinja" import librato with context %}
{%- set role = salt['grains.get']('roles', '') %}

{%- for ele in ['petabucket-test', 'petabucket-prod', 'zookeeper', 'vpn'] %}
{%- if role == ele %}
{{ librato.plugin_config_path }}/processes.conf:
  file.managed:
    - template: jinja
    - user: root
    - group: root
    - mode: 0644
    - source: salt://librato/files/processes.conf.jinja
    - context:
      process: {{ librato.processes.name }}
      processmatch: {{ librato.processes.processmatch }}
{%- endif %}
{%- endfor %}

include:
  - librato.agent
